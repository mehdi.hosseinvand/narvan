# narvan_test
$ git clone https://gitlab.com/mehdi.hosseinvand/narvan.git
$ cd narvan
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ cd project
$ python manage.py runserver
# send json data with postman application:
# operators include  = ["factorial", "fibonacci", "ackermann"]

{
    "n":5,
    "opr":"fibonacci"

}
{
    "n":5,
    "opr":"factorial"

}
{
    "n":5,
    "m":3,
    "opr":"ackermann"

}